# ---------------------------------------------------------------
# Build psplash
# ---------------------------------------------------------------

root-verify:
	@if [ $(UID) -ne 0 ]; then \
		$(MSG11) "You must be root." $(EMSG); \
		exit 1; \
	fi

opkg-verify:
	@if [ "$(OPKG_DIR)" = "" ] || [ ! -f $(OPKG_DIR)/opkg-build ]; then \
		$(MSG11) "Can't find opkg-build.  Try setting OPKG= to the directory it lives in." $(EMSG); \
		exit 1; \
	fi

build-verify:
	@if [ "$(CROSS_COMPILER)" = "" ] || [ ! -f $(CROSS_COMPILER) ]; then \
		$(MSG11) "Can't find cross toolchain.  Try setting XI= on the command line." $(EMSG); \
		exit 1; \
	fi
	@if [ "$(SD)" = "" ] || [ ! -d $(SD) ]; then \
		$(MSG11) "Can't find staging tree.  Try setting SD= on the command line." $(EMSG); \
		exit 1; \
	fi

# Retrieve package
.$(PSPLASH_T)-get $(PSPLASH_T)-get: 
	@if [ ! -f $(ARCDIR)/$(PSPLASH_ARCHIVE) ]; then \
		$(MSG) "================================================================"; \
		$(MSG2) "Retrieving files" $(EMSG); \
		$(MSG) "================================================================"; \
		mkdir -p $(ARCDIR); \
		cd $(ARCDIR) && git clone $(PK_URL); \
	else \
		$(MSG3) "PSPLASH source is cached" $(EMSG); \
	fi
	@touch .$(subst .,,$@)

.$(PSPLASH_T)-get-patch $(PSPLASH_T)-get-patch: .$(PSPLASH_T)-get
	@touch .$(subst .,,$@)

# Unpack packages
.$(PSPLASH_T)-unpack $(PSPLASH_T)-unpack: .$(PSPLASH_T)-get-patch
	@mkdir -p $(BLDDIR)
	$(UNPACK_CMD)
	@touch .$(subst .,,$@)

# Apply patches
.$(PSPLASH_T)-patch $(PSPLASH_T)-patch: .$(PSPLASH_T)-unpack
	@if [ -d $(DIR_PATCH) ]; then \
		$(MSG) "================================================================"; \
		$(MSG2) "Patching source" $(EMSG); \
		$(MSG) "================================================================"; \
		for patchname in `ls -1 $(DIR_PATCH)/*.patch`; do \
			$(MSG3) Applying $$patchname $(EMSG); \
			cd $(PSPLASH_SRCDIR) && patch -Np1 -r - < $$patchname; \
		done; \
	fi
	@touch .$(subst .,,$@)

.$(PSPLASH_T)-init $(PSPLASH_T)-init: 
	@make build-verify
	@make .$(PSPLASH_T)-patch
	@touch .$(subst .,,$@)

.$(PSPLASH_T)-config $(PSPLASH_T)-config: 
	@$(MSG) "================================================================"
	@$(MSG2) "Configuring source" $(EMSG)
	@$(MSG) "================================================================"
	cd $(PSPLASH_SRCDIR) && autoreconf -i
	cd $(PSPLASH_SRCDIR) && \
		PATH=$(XCC_PREFIXDIR)/bin:$(PATH) \
    	CFLAGS=-I$(SD)/usr/include \
    	LDFLAGS="-L$(SD)/usr/lib -L$(SD)/lib --sysroot=$(SD)/" \
    	./configure --host=$(XCC_PREFIX)

.$(PSPLASH_T)-logo $(PSPLASH_T)-logo: 
	@$(MSG) "================================================================"
	@$(MSG2) "Updating logo" $(EMSG)
	@$(MSG) "================================================================"
	cp $(PSPLASH_LOGODIR)/$(PSPLASH_LOGO) $(PSPLASH_SRCDIR)
	cp $(PSPLASH_LOGODIR)/$(PSPLASH_BAR) $(PSPLASH_SRCDIR)
	cd $(PSPLASH_SRCDIR) && \
		./make-image-header.sh $(PSPLASH_LOGO) POKY && \
		mv `echo $(PSPLASH_LOGO) | cut -f1 -d"."`-img.h psplash-poky-img.h
	cd $(PSPLASH_SRCDIR) && \
		./make-image-header.sh $(PSPLASH_BAR) BAR && \
		mv `echo $(PSPLASH_BAR) | cut -f1 -d"."`-img.h psplash-bar-img.h
	cd $(PSPLASH_SRCDIR) && rm $(PSPLASH_LOGO) $(PSPLASH_BAR)

$(PSPLASH_T): .$(PSPLASH_T)

.$(PSPLASH_T): .$(PSPLASH_T)-init 
	@make --no-print-directory $(PSPLASH_T)-logo
	@make --no-print-directory $(PSPLASH_T)-config
	@$(MSG) "================================================================"
	@$(MSG2) "Building PSPLASH" $(EMSG)
	@$(MSG) "================================================================"
	@cd $(PSPLASH_SRCDIR) && \
		PATH=$(XCC_PREFIXDIR)/bin:$(PATH) \
		make

# Build the package
	@touch .$(subst .,,$@)

$(PSPLASH_T)-files:
	@$(MSG) "================================================================"
	@$(MSG2) "PSPLASH Files" $(EMSG)
	@$(MSG) "================================================================"
	@ls -l $(PSPLASH_SRCDIR)

# Package it as an opkg 
pkg $(PSPLASH_T)-pkg: .$(PSPLASH_T) 
	@make --no-print-directory root-verify opkg-verify
	@mkdir -p $(PKGDIR)/install
	@cd $(PSPLASH_SRCDIR) && \
		PATH=$(XCC_PREFIXDIR)/bin:$(PATH) \
		DESTDIR=$(PKGDIR)/install \
		make install
	@mkdir -p $(PKGDIR)/opkg/psplash/CONTROL
	@mkdir -p $(PKGDIR)/opkg/psplash/etc/init.d/functions.d
	@cp -ar $(SRCDIR)/scripts/S??* $(PKGDIR)/opkg/psplash/etc/init.d/
ifeq ($(TFT),1)
	@mv $(PKGDIR)/opkg/psplash/etc/init.d/S01apsplash $(PKGDIR)/opkg/psplash/etc/init.d/S12apsplash
endif
	@cp -ar $(SRCDIR)/scripts/psplash.f $(PKGDIR)/opkg/psplash/etc/init.d/functions.d
	@cp -ar $(PKGDIR)/install/* $(PKGDIR)/opkg/psplash/
	@cp $(SRCDIR)/opkg/control $(PKGDIR)/opkg/psplash/CONTROL/control
	@cp $(SRCDIR)/opkg/debian-binary $(PKGDIR)/opkg/psplash/CONTROL/debian-binary
	@cp $(SRCDIR)/opkg/postinst $(PKGDIR)/opkg/psplash/CONTROL/postinst
	@sed -i 's%\[VERSION\]%'`cat $(TOPDIR)/version.txt`'%' $(PKGDIR)/opkg/psplash/CONTROL/control
	@chown -R root.root $(PKGDIR)/opkg/psplash/
	@cd $(PKGDIR)/opkg/ && $(OPKG_DIR)/opkg-build -O psplash
	@cp $(PKGDIR)/opkg/*.opk $(PKGDIR)/
	@rm -rf $(PKGDIR)/opkg

# Clean the packaging
pkg-clean $(PSPLASH_T)-pkg-clean:
	@if [ "$(PKGDIR)" != "" ] && [ -d "$(PKGDIR)" ]; then rm -rf $(PKGDIR); fi

# Clean out a cross compiler build but not the CT-NG package build.
$(PSPLASH_T)-clean: $(PSPLASH_T)-pkg-clean
	@if [ "$(PSPLASH_SRCDIR)" != "" ] && [ -d "$(PSPLASH_SRCDIR)" ]; then \
		cd $(PSPLASH_SRCDIR) && make distclean; \
	fi
	@rm -f .$(PSPLASH_T) 

# Clean out everything associated with PSPLASH
$(PSPLASH_T)-clobber: root-verify
	@rm -rf $(PKGDIR) 
	@rm -rf $(PSPLASH_SRCDIR) 
	@rm -f .$(PSPLASH_T)-config .$(PSPLASH_T)-init .$(PSPLASH_T)-patch \
		.$(PSPLASH_T)-unpack .$(PSPLASH_T)-get .$(PSPLASH_T)-get-patch
	@rm -f .$(PSPLASH_T) 

