#!/bin/sh
# Utility functions for psplash
# --------------------------------------
# Globals
PSPLASH_STATE=1
PSPLASH_PERC=/etc/.psplash
PSPLASH_BUMP=/etc/.psplashbump
PSPLASH=/usr/local/bin/psplash
PSPLASH_WRITE=/usr/local/bin/psplash-write
PSPLASH_FIFO_DIR=/tmp
PSPLASH_STATUS=/etc/init.d/rcstatus

# Test if psplash is installed
isSplashInstalled()
{
    if [ -f $PSPLASH ]
    then
        return 1
    else
        return 0
    fi
}

# If psplash is set to 0 in the kernel args
# then the splash is disabled.
# Otherwise it is enabled.
setSplashState()
{
    STATE=$(getArgValue psplash)
    if [ "$STATE" = "0" ]; then
        PSPLASH_STATE=0
        return 0
    fi
    isSplashInstalled
    PSPLASH_STATE=$?
}

# Setup the splash environment
initSplash()
{
    setSplashState
    if [ $PSPLASH_STATE -eq 0 ]; then
        return 0
    fi

    # Init the current percent
    echo 0 > $PSPLASH_PERC

    # Set the bump amount
    bump=$(numScripts)
    bump=$(( 100 / $bump ))
    echo $bump > $PSPLASH_BUMP
    if [ ! -e $PSPLASH_BUMP ]; then
        echo 1 > $PSPLASH_BUMP
    fi
    return 1
}

# Find the appropriate framebuffer device for 
# the splash logo.  We always choose the highest
# number device (and there can only be two, at most).
getSplashFB()
{
    if [ -e /dev/fb1 ]; then
        echo "/dev/fb1" | cut -c8-
        return 0
    elif [ -e /dev/fb0 ]; then
        echo "/dev/fb0" | cut -c8-
        return 0
    fi
    echo
}

# Update splash percentage
# If provided, first arg is a splash message.
bumpSplash()
{
    local perc
    local bump
    local update

    # if [ "$1" != "" ]
    # then
    #     $PSPLASH_WRITE "MSG $1"
    # fi

    perc=`cat $PSPLASH_PERC`
    bump=`cat $PSPLASH_BUMP`
    update=`expr $perc + $bump`
    $PSPLASH_WRITE "PROGRESS $perc"
    # echo "$1: Perc=$perc, Bump=$bump, Update=$update" >> "${PSPLASH_STATUS}"
    echo $update > $PSPLASH_PERC
}

# Remove the splash setup so the next boot can use it.
clearSplash()
{
    rm $PSPLASH_PERC
    rm $PSPLASH_BUMP
}

# Initialize splash environment
# Gets run once on every boot but doesn't need to be run again.
if [ ! -f "$PSPLASH_PERC" ]; then
    initSplash
fi
